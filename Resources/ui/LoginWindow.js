exports.LoginWindow = function(args) {
	var instance = Ti.UI.createWindow(args);

	var email = Titanium.UI.createTextField({
	    hintText : 'Email address',
	    keyboardType: Titanium.UI.KEYBOARD_EMAIL,
	    height : 35,
	    width : 300,
	    top : 10,
	    borderStyle : Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	    returnKeyType: Titanium.UI.RETURNKEY_NEXT
	});
	
	var password = Titanium.UI.createTextField({
	    hintText : 'Password',
	    height : 35,
	    width : 300,
	    top : 55,
	    passwordMask: true,
	    borderStyle : Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	    returnKeyType: Titanium.UI.RETURNKEY_DONE
	});
	
	var btnLogin = Ti.UI.createButton({
		title: 'Login',
		color: '#2873ba',
		backgroundImage: 'none',
		borderRadius: 8,
		borderColor: '#3787d3',
		borderWidth: 1,
		font: {
			fontSize: 22,
			fontWeight: 'bold'			
		},
		selectedColor: 'none',
		selectedImage: 'none',
	    backgroundGradient: globals.styles.gradient('ltBlue'),
		height: 50,
		width: 300,
		top: 100,
		left: 10
	});
	
	var lblForgotPassword = Titanium.UI.createLabel({
	    text:'I forgot my password.',
	    top: 170,
	    height: 'auto',
	    width: 300,
	    shadowColor:'#fff',
	    shadowOffset:{ x:1, y:1},
	    color: '#333',
	    textAlign: 'right'
	});
	
	email.addEventListener('return', function(e) {
		password.focus();
	});
	
	password.addEventListener('return', function(e) {
		submitLoginForm();
	});
	
	btnLogin.addEventListener('click',function(e) {
   		submitLoginForm();
	});
	
	lblForgotPassword.addEventListener('click', function(e) {
		globals.loginTab.setActiveTab(1);
	});
	
	function submitLoginForm() {
		var emailValue = email.getValue();
   		var passwordValue = password.getValue();
   		
   		if (emailValue == '' || passwordValue == '') {
   			var alert = Ti.UI.createAlertDialog({
				message: 'Please enter an email address and password.',
				ok: 'Okay',
				title: 'Oops!'
			}).show();
   		} else {
   			globals.login.tryLogin(emailValue,passwordValue);
   			email.blur();
   			password.blur();
   		}
	}
	
	instance.add(email);
	instance.add(password);
	instance.add(btnLogin);
	instance.add(lblForgotPassword);

	return instance;
};