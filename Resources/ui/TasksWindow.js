exports.TasksWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	
	var TasksUpdateWindow = require('ui/TasksUpdateWindow');
	var TasksListWindow = require('ui/TasksListWindow');
				
	var tasksUpdateWindow = new TasksUpdateWindow({title: 'New Task', backgroundColor: globals.appBGColor, barColor: globals.navBarColor, tabBarHidden: true});
	
	var btnNewTask = Ti.UI.createButton({
		title: 'New Task',
 		image: 'images/plus-white.png'
 	});
 	btnNewTask.addEventListener('click', function() {
 		tasksUpdateWindow.open({modal: true});
 	});
 	instance.setRightNavButton(btnNewTask);
	
	var tableView = Ti.UI.createTableView({
		style: Titanium.UI.iPhone.TableViewStyle.GROUPED,
		backgroundColor:'transparent',
		rowBackgroundColor:'white'
	});
	
	var data = [], row, l1, lblNumberOverdue, lblNumberToday, lblNumberThisWeek, lblNumberFuture;
	row = Ti.UI.createTableViewRow({height: 40, hasChild: true, type: 'TasksOverdue' });
	l1 = Ti.UI.createLabel({text: 'Overdue', left: 40, font: {fontSize: 16, fontWeight: 'bold'}});
	l2 = Ti.UI.createLabel({
		backgroundImage: 'images/alert_triangle_red.png',
		width: 24,
		height: 24,
		left: 8,
	});
	labels.lblNumberOverdue = Ti.UI.createLabel(globals.styles.tableViewBubble());
	row.add(l1,l2,labels.lblNumberOverdue);
	data.push(row);
	
	row = Ti.UI.createTableViewRow({height: 40, hasChild: true, type: 'TasksToday' });
	l1 = Ti.UI.createLabel({text: 'Today', left: 40, font: {fontSize: 16, fontWeight: 'bold'}});
	l2 = Ti.UI.createLabel({
		backgroundImage: 'images/calendar_red.png',
		width: 24,
		height: 24,
		left: 8,
	});
	labels.lblNumberToday = Ti.UI.createLabel(globals.styles.tableViewBubble());
	row.add(l1,l2,labels.lblNumberToday);
	data.push(row);
	
	row = Ti.UI.createTableViewRow({height: 40, hasChild: true, type: 'TasksTomorrow' });
	l1 = Ti.UI.createLabel({text: 'Tomorrow', left: 40, font: {fontSize: 16, fontWeight: 'bold'}});
	l2 = Ti.UI.createLabel({
		backgroundImage: 'images/calendar_red.png',
		width: 24,
		height: 24,
		left: 8,
	});
	labels.lblNumberTomorrow = Ti.UI.createLabel(globals.styles.tableViewBubble());
	row.add(l1,l2,labels.lblNumberTomorrow);
	data.push(row);
	
	row = Ti.UI.createTableViewRow({height: 40, hasChild: true, type: 'TasksThisWeek' });
	l1 = Ti.UI.createLabel({text: 'This Week', left: 40, font: {fontSize: 16, fontWeight: 'bold'}});
	l2 = Ti.UI.createLabel({
		backgroundImage: 'images/calendar_red.png',
		width: 24,
		height: 24,
		left: 8,
	});
	labels.lblNumberThisWeek = Ti.UI.createLabel(globals.styles.tableViewBubble());
	row.add(l1,l2,labels.lblNumberThisWeek);
	data.push(row);
	
	row = Ti.UI.createTableViewRow({height: 40, hasChild: true,  type: 'TasksNextWeek'});
	l1 = Ti.UI.createLabel({text: 'Next Week', left: 40, font: {fontSize: 16, fontWeight: 'bold'}});
	l2 = Ti.UI.createLabel({
		backgroundImage: 'images/calendar_red.png',
		width: 24,
		height: 24,
		left: 8,
	});
	labels.lblNumberNextWeek = Ti.UI.createLabel(globals.styles.tableViewBubble());
	row.add(l1,l2,labels.lblNumberNextWeek);
	data.push(row);
	
	row = Ti.UI.createTableViewRow({height: 40, hasChild: true, type: 'TasksIncomplete' });
	l1 = Ti.UI.createLabel({text: 'All Incomplete Tasks', left: 40, font: {fontSize: 16, fontWeight: 'bold'}});
	l2 = Ti.UI.createLabel({
		backgroundImage: 'images/ok_grey.png',
		width: 24,
		height: 24,
		left: 8,
	});
	labels.lblNumberIncomplete = Ti.UI.createLabel(globals.styles.tableViewBubble());
	row.add(l1,l2,labels.lblNumberIncomplete);
	data.push(row);
	
	row = Ti.UI.createTableViewRow({height: 40, hasChild: true, type: 'TasksArchived' });
	l1 = Ti.UI.createLabel({text: 'Archived Tasks', left: 40, font: {fontSize: 16, fontWeight: 'bold'}});
	l2 = Ti.UI.createLabel({
		backgroundImage: 'images/box_opened_brown.png',
		width: 24,
		height: 24,
		left: 8,
	});
	labels.lblNumberArchived = Ti.UI.createLabel(globals.styles.tableViewBubble());
	row.add(l2,l1,labels.lblNumberArchived);
	data.push(row);
	
	tableView.setData(data);
	
	tableView.addEventListener('click', function(e) {
		var row = e.row;
		var rowdata = e.rowData;
		var rowIndex = e.index;
		var tasksListWindow = new TasksListWindow({ taskType: row.type, backgroundColor: globals.appBGColor, barColor: globals.navBarColor });
		
		if (row.type =='TasksArchived') {
			globals.tasks.getAllUserTasksByStatus('archive',tasksListWindow);
		} else if (row.type == 'TasksIncomplete') {
			globals.tasks.getAllUserTasksByStatus('incomplete',tasksListWindow);
		} else {	
			globals.tabs.currentTab.open(tasksListWindow,{animated:true});
		}	
	});
	
	instance.add(tableView);
	
	instance.addEventListener('focus', function(e) {
		if (Object.keys(globals.usertasks).length === 0) {
			globals.tasks.getTasksForUser();
		}	
	});
	instance.add(globals.ai);
	return instance;
};