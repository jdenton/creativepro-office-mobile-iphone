exports.AccountWindow = function(args) {
	var instance = Ti.UI.createWindow(args);

	var btnLogout = Ti.UI.createButton({
		title: 'Log Out',
		color: '#2873ba',
		backgroundImage: 'none',
		borderRadius: 8,
		borderColor: '#3787d3',
		borderWidth: 1,
		font: {
			fontSize: 22,
			fontWeight: 'bold'			
		},
		selectedColor: 'none',
		selectedImage: 'none',
	    backgroundGradient: globals.styles.gradient('ltBlue'),
		height: 50,
		width: 300,
		top: 20,
		left: 10
	});
	
	btnLogout.addEventListener('click', function(e) {
		var appDB = Ti.Database.open('cpo');
		sql = "DELETE FROM cpo_login";
		appDB.execute(sql); 
		appDB.close();
		
		globals.AUTH_KEY = null;
		globals.USER_TYPE = null;
		globals.tabs.hide();
		globals.loginTab.open();
		globals.loginTab.setActiveTab(0);
	});
	
	instance.add(btnLogout);

	return instance;
};